package it.com.atlassian.jira.ext.calendar;

import com.atlassian.jira.ext.calendar.HtmlCalendarConfiguration;
import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.locator.XPathLocator;
import com.atlassian.jira.functest.framework.navigation.IssueNavigation;
import com.atlassian.jira.functest.framework.navigation.IssueNavigationImpl;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.meterware.httpunit.HttpUnitOptions;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import org.hamcrest.CoreMatchers;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertThat;

public class TestCalendarPlugin extends FuncTestCase
{
//    private IssueNavigation issueNavigation;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestCalendarPluginData.xml");
        HttpUnitOptions.setScriptingEnabled(true);
//        issueNavigation = new IssueNavigationImpl(getTester(), getEnvironmentData());
    }

//    public void testICalExport() throws IOException, ParserException
//    {
//        // Create a current issue to make sure that there is something in the calendar
//        DateFormat dueDateFormat = new SimpleDateFormat("dd/MMM/yyyy");
//        issueNavigation.setDueDate(issueNavigation.createIssue("Test", ISSUE_TYPE_BUG, "New Issue created summary"), dueDateFormat.format(new Date()));
//
//        goToCalendarTabPanel("TST");
//
//        tester.clickLinkWithText("Export in ICal format");
//        Calendar cal;
//        String text;
//
//        text = tester.getDialog().getResponse().getText();
//        assertNotNull(text);
//        // ToDo: Find out why the Calendar builder can't parse these calendar files
//        CalendarBuilder calBuilder = new CalendarBuilder();
//        cal = calBuilder.build(new StringReader(text));
//
//        assertNotNull(cal);
//    }

    public void testProjectTabPanel()
    {
        navigation.gotoPage(generateHtmlServletUrl("042006", DocumentConstants.ISSUE_DUEDATE, "true", HtmlCalendarConfiguration.PROJECT_TAB_CONTEXT, "10000"));
        goToCalendarTabPanel("TST");
        tester.assertTextPresent("April 2006");

        XPathLocator locator = new XPathLocator(tester, "//a[@class='version']");
        assertThat(locator.getText(), CoreMatchers.containsString("version 2"));
        assertThat(locator.getText(), CoreMatchers.containsString("Project: TST"));

        tester.assertTextPresent("Versions:");
        tester.assertLinkPresentWithText("On");

        // Don't display versions
        navigation.gotoPage(generateHtmlServletUrl("042006", DocumentConstants.ISSUE_DUEDATE, "false", HtmlCalendarConfiguration.PROJECT_TAB_CONTEXT, "10000"));
        goToCalendarTabPanel("TST");
        tester.assertTextPresent("April 2006");

        locator = new XPathLocator(tester, "//a[@class='version']");
        assertEquals("", locator.getText()); // Because versions are turned off

        tester.assertTextPresent("Versions:");
        tester.assertLinkPresentWithText("On");

        // Try changing months
        navigation.gotoPage(generateHtmlServletUrl("052006", DocumentConstants.ISSUE_DUEDATE, "false", HtmlCalendarConfiguration.PROJECT_TAB_CONTEXT, "10000"));
        goToCalendarTabPanel("TST");
        tester.assertTextPresent("May 2006");
    }

    public void goToCalendarTabPanel(String project)
    {
        navigation.browseProjectTabPanel(project, "issuecalendar");
    }

    public String generateHtmlServletUrl(String month, String dateFieldName, String displayVersions, String context, String key)
    {
        String url = "/rest/calendar-plugin/1.0/calendar/htmlcalendar.html?searchRequestId=10000&month=" + month
                + "&" + (HtmlCalendarConfiguration.PROJECT_TAB_CONTEXT.equals(context) ? "projectId" : "portletId") + "=" + key
                + "&dateFieldName=" + dateFieldName
                + "&displayVersions=" + displayVersions
                + "&numOfIssueIcons=10&context=" + context;

        return url;
    }

    private static String getTabPanelUrl(String project)
    {
        return "/browse/" + project + "?selectedTab=com.atlassian.jira.ext.calendar:issuecalendar-panel";
    }

//    // JCAL-154
//    public void testCheckThatOnlyOneIssueIsShowingForTheSelectedDay()
//    {
//        administration.project().addProject("Game", "GAME", "admin");
//        HttpUnitOptions.setScriptingEnabled(true);
//
//        GregorianCalendar calendar = new GregorianCalendar();
//        DateFormat dueDateFormat = new SimpleDateFormat("dd/MMM/yyyy");
//        Date today = calendar.getTime();
//        issueNavigation.setDueDate(issueNavigation.createIssue("Game", ISSUE_TYPE_BUG, "Bug 1"), dueDateFormat.format(today));
//
//        calendar.add(calendar.DAY_OF_MONTH, 1);
//        Date tomorrow = calendar.getTime();
//        issueNavigation.setDueDate(issueNavigation.createIssue("Game", ISSUE_TYPE_BUG, "Bug 2"), dueDateFormat.format(tomorrow));
//        issueNavigation.setDueDate(issueNavigation.createIssue("Game", ISSUE_TYPE_BUG, "Bug 3"), dueDateFormat.format(tomorrow));
//
//        goToCalendarTabPanel("GAME");
//
//        tester.clickLinkWithText("1");
//
//        // Check if GAME-1 issue is there in the page
//        tester.assertTextPresent("Issue Navigator");
//        tester.assertTextPresent("GAME-1");
//        tester.assertTextPresent("Bug 1");
//        tester.assertTextPresent("Unresolved");
//        tester.assertTextPresent(dueDateFormat.format(today).toString());
//
//        // And check to be sure GAME-2 issue is not there
//        tester.assertTextNotPresent("GAME-2");
//        tester.assertTextNotPresent("Bug 2");
//        tester.assertTextNotPresent(dueDateFormat.format(tomorrow).toString());
//    }
}